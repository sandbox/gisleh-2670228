<?php

/**
 * @file
 * Field formatters for Views RSS: Core Elements module.
 */

/**
 * Implements hook_field_formatter_info().
 */
function rss_scald_x_field_formatter_info() {
  $formatters = array(
    'enclosure' => array(
      'label' => t('Scald RSS <enclosure> element'),
      'field types' => array('atom_reference'),
    ),
  );
  return $formatters;
}


/**
 * Implements hook_field_formatter_view().
 *
 * This is a dirty trick here. Essentially, we do not want to call a theme
 * function from here, as it should be called from within a view (amongst
 * other to have $view object in $variables). Therefore here we want to
 * return value only, hence use of array('#markup' => $value). However,
 * in some cases it won't be just a simple string value to return,
 * sometimes we'd want to return an array (for example value with
 * additional arguments) - hence the need to serialize it (plus add
 * "serialized" string at the beginning so that our field preprocess
 * function template_preprocess_views_view_views_rss_field() is able
 * to recognize it as serialized array and treat accordingly.
 *
 * Any better ideas?
 */
function rss_scald_x_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = NULL;
  foreach ($items as $delta => $item) {
    // Inside a view item may contain NULL data. In that case, just return.
    if ($field['type'] == 'atom_reference') {
      $field_name = $field['field_name'];
      // Get the sid from the atom and load it.
      $entity_lang = $entity->language;
      $ent_tmp = $entity->$field_name;
      $sid = $ent_tmp[LANGUAGE_NONE][0]['sid'];
      if (isset($ent_tmp[$entity_lang])) {
        $sid = $ent_tmp[$entity_lang][0]['sid'];
      }
      $atom_info = scald_atom_load($sid);
      $atom_lang = $atom_info->language;
      // Fill the <enclosure> from the atom field.
      if ($atom_info->type == 'image') {
        $item['uri'] = $atom_info->scald_thumbnail[$atom_lang][0]['uri'];
        $item['filesize'] = $atom_info->scald_thumbnail[$atom_lang][0]['filesize'];
        $item['filemime'] = $atom_info->scald_thumbnail[$atom_lang][0]['filemime'];
      }
    }
    $uri = file_create_url($item['uri']);
    // XML element array in format_xml_elements() format.
    $rss_element = array(
      'key' => 'enclosure',
      'attributes' => array(
        'url' => $uri,
        'length' => $item['filesize'],
        'type'   => $item['filemime'],
      ),
    );

    $element[$delta] = array(
      '#item' => $item,
      '#markup' => format_xml_elements(array($rss_element)),
      '#rss_element' => $rss_element,
    );

  }
  return $element;
}
