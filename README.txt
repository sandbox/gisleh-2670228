

CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration


INTRODUCTION
------------
Current Maintainer: Seb Bd <sepro@smile.com>

- This is a extension module to the Views RSS module.
- Natively, the views RSS module support the insertion of images in
  the enclosure element of the RSS feed. But, if you use the scald
  media library as multimedia content manager, the image field would
  not be a simple image but a scald "atom reference" image.
- This extension module add the same functionality if the image is a
  scald atom reference image and not a simple image


REQUIREMENTS
------------

The views module (https://www.drupal.org/project/views) and views rss
module (https://www.drupal.org/project/views_rss) are required.


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

Prerequisite
    You have to configure a views, a feed in the views and to
    configure the feed first. (detailed information in views module
    and views rss module)

Once views and views rss are functionnal, please follow these steps :

    In the views configuration
    * [Field part] Add the desired image field
    * [Field part] Click on the image element to configure the views field 
    * [Formatter part] Select the "RSS enclosure element" formatter.

    Associate your image to the RSS feed - Field  
    * [Format part] Click on parameters -> Item elements:Core ->
      select the image field for the enclosure item


DO


git branch -d 7.x-1.x-dev

git push -u origin 7.x-1.x

Click on Edit -> Default branch
Check 7.x-1.x
Click save


git remote set-head origin 7.x-1.x
git push origin --delete 7.x-1.x-dev
